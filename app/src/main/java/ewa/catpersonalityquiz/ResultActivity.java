package ewa.catpersonalityquiz;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import ewa.catpersonalityquiz.Quizzes.Result;



public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ImageView resultImage = (ImageView) findViewById(R.id.resultImage);
        TextView resultHeading = (TextView) findViewById(R.id.headingText);
        TextView categoryDisplay = (TextView) findViewById(R.id.categoryTextView);
        TextView resultBodyText = (TextView) findViewById(R.id.bodyTextView);
        Button againButton = (Button) findViewById(R.id.againButton);


        Intent intent = getIntent();
        String catName = intent.getStringExtra(getString(R.string.key_name_cat));
        if (catName.equals("your cat")){
            catName = "Your cat";
        }
        int resultNumber = intent.getIntExtra(getString(R.string.key_name_total), 0);
        //TODO: implement a dialog in case result is 0 or below 0 to the effect of whoops you got here by mistake, back to beginning?
        Result result = getQuizResult(resultNumber);
        Drawable drawable = ContextCompat.getDrawable(this, result.getResultImageID());
        resultImage.setImageDrawable(drawable);

        resultHeading.setText(result.getResultHeading());
        YoYo.with(Techniques.FadeIn)
                .duration(900)
                .playOn(resultHeading);

        categoryDisplay.setText(catName + " is " + result.getResultCategory());
        YoYo.with(Techniques.FadeIn)
                .duration(900)
                .playOn(categoryDisplay);

        resultBodyText.setText(result.getResultText());
        YoYo.with(Techniques.FadeIn)
                .duration(900)
                .playOn(resultBodyText);










        againButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(2, null);
                finish();
            }
        });
    }

    //should this be its own class? or possibly in the result class?

    private Result getQuizResult(int resultNumber) {
        Result result = new Result();
        if (resultNumber >= 10 && resultNumber <=15){
            result.setResultHeading("Um?");
            result.setResultCategory("not a cat?");
            result.setResultText("Are you sure your cat is a cat? Only they do not sound very cat. Maybe some other animal? Like a dog? Or a rock? Or a ghost?");
            result.setResultImageID(R.drawable.kittytransparent);
        }
        else if (resultNumber >=16 && resultNumber <=25){
            result.setResultHeading("Congratulations!");
            result.setResultCategory("an asshole");
            result.setResultText("Your cat is a cat. It is quite cat.");
            result.setResultImageID(R.drawable.kittybase);
        }
        else if (resultNumber >=26 && resultNumber <=34){
            result.setResultHeading("Congratulations?");
            result.setResultCategory("a huge asshole");
            result.setResultText("Your cat is a cat. It is quite cat. Perhaps it is a little...too cat?");
            result.setResultImageID(R.drawable.kittyteef);
        }
        else if (resultNumber >=35 && resultNumber <=40){
            result.setResultHeading("Condolences!");
            result.setResultCategory("some sort of demon");
            result.setResultText("Your cat is not a cat. It is a demon that happens to respond to catnip. Good luck with that!");
            result.setResultImageID(R.drawable.kittydemon);
        }


        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
