package ewa.catpersonalityquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import ewa.catpersonalityquiz.Quizzes.Question;
import ewa.catpersonalityquiz.Quizzes.Quiz;

//TODO: practice loading screen by "loading" questions

public class QuizActivity extends AppCompatActivity {
    //layout variables
    private String myCatName;
    private TextView myQuestionNumberView;
    private TextView myQuestionView;
    private RadioGroup myOptionGroup;
    private RadioButton myOption1;
    private RadioButton myOption2;
    private RadioButton myOption3;
    private RadioButton myOption4;
    private boolean isChecked = false;
    private Button mySubmitButton;
    private Button myPreviousButton;

    //quiz variables
    private Question myCurrentQuestion;
    private int myQuestionResult = 0;
    private Quiz myQuiz = new Quiz();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        int questionNmr = 0;

        //retrieve cat name from previous activity
        Intent intent = getIntent();
        myCatName = intent.getStringExtra(getString(R.string.key_name_cat));
        if (myCatName.isEmpty()){
            myCatName = "your cat";
        }

        //assigned layout variables to actual views
        myQuestionNumberView = (TextView) findViewById(R.id.questionNmrView);
        myQuestionView = (TextView) findViewById(R.id.questionTextView);
        myOptionGroup = (RadioGroup)findViewById(R.id.optionGroup);
        myOption1 = (RadioButton) findViewById(R.id.option1);
        myOption2 = (RadioButton) findViewById(R.id.option2);
        myOption3 = (RadioButton) findViewById(R.id.option3);
        myOption4 = (RadioButton) findViewById(R.id.option4);
        mySubmitButton = (Button) findViewById(R.id.submitButton);
        myPreviousButton = (Button) findViewById(R.id.previousButton);


        //loads question into views with class below
        loadQuestion(questionNmr);


    }

    //populates layout variables with the actual questions
    private void loadQuestion(final int questionNmr){

        //retrieve current question
        myCurrentQuestion = myQuiz.getQuestion(questionNmr);

        //iterate to next question - do it here so that the Question number display starts from 1 instead of 0]
        final int nextQuestion = questionNmr + 1;

        //constructs question view, populates text with cat's name
        myQuestionNumberView.setText("Question " + (nextQuestion));
        String questionText = myCurrentQuestion.getQuestionText();
        questionText = String.format(questionText, myCatName);
        myQuestionView.setText(questionText);
        myOption1.setText(myCurrentQuestion.getOption1());
        myOption2.setText(myCurrentQuestion.getOption2());
        myOption3.setText(myCurrentQuestion.getOption3());
        myOption4.setText(myCurrentQuestion.getOption4());

        /*Previous question button should only be visible after the first question
         *So, visibility is set by checking to make sure that the question number is not 0
         *I have no idea why it insists that questionNmr is never zero, but it is wrong. and it works. So there.
        */
        if (questionNmr == 0)
            myPreviousButton.setVisibility(View.INVISIBLE);
        else if (questionNmr != 0 && myPreviousButton.getVisibility() == View.INVISIBLE)
            myPreviousButton.setVisibility(View.VISIBLE);

        //for last question only
        if (nextQuestion == myQuiz.getQuizLength()){
            mySubmitButton.setText("Finish Quiz");
        }
        //waits for button click
        mySubmitButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                //checks to make sure an answer has been selected, if not, displays toast prompting selection
                                                if (isChecked)

                                                    //checks to make sure there is a next question, if not, displays result in toast (for now)
                                                    if (nextQuestion < myQuiz.getQuizLength()) {
                                                        //sets current questions answer value using radio button. total is now added up in Quiz class using getQuizResult method
                                                        myCurrentQuestion.setAnswerValue(myQuestionResult);
                                                        //clears radio group and resets isChecked to false for next question
                                                        myOptionGroup.clearCheck();
                                                        isChecked = false;
                                                        //loads next question
                                                        loadQuestion(nextQuestion);
                                                    } else {
                                                        myCurrentQuestion.setAnswerValue(myQuestionResult);
                                                        endQuiz(myQuiz.getQuizResult());
                                            }
                                                else {
                                                    Toast.makeText(QuizActivity.this, "Please Choose An Answer", Toast.LENGTH_SHORT).show();
                                                }


                                            }
                                        }
        );

        /*Previous question button listener. On click, it subtracts one from the questionNmr,
         *clears the radio buttons (in case someone chose an answer before deciding to return to the previous question)
         *and loads the question with the new question number.
         */
        myPreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int previousQuestion = questionNmr-1;
                myOptionGroup.clearCheck();
                isChecked = false;
                loadQuestion(previousQuestion);

            }
        });

    }

    //listens for radio button selection (assigned in layout xml file)
    //assigns a result value based on which button is selected

    public void onRadioButtonClicked(View view){
        isChecked = ((RadioButton) view).isChecked();

        switch(view.getId()){
            case R.id.option1:
                if (isChecked){
                    myQuestionResult = 1;
                }
                break;
            case R.id.option2:
                if (isChecked){
                    myQuestionResult = 2;
                }
                break;
            case R.id.option3:
                if (isChecked){
                    myQuestionResult = 3;
                }
                break;
            case R.id.option4:
                if (isChecked){
                    myQuestionResult = 4;
                }
                break;
        }
    }

    private void endQuiz(int quizTotal){
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra(getString(R.string.key_name_total), quizTotal);
        intent.putExtra(getString(R.string.key_name_cat), myCatName);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
            if(requestCode == 1){
                if(resultCode == 2){
                    finish();
                }
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
