package ewa.catpersonalityquiz.Quizzes;

/**
 * Created by Ewa on 8/29/2015.
 * Holds the actual question components and constructors
 */
public class Question {
    private String myQuestionText;
    private String myOption1;
    private String myOption2;
    private String myOption3;
    private String myOption4;
    private int myAnswerValue;

    public Question(String questionText, String option1, String option2, String option3, String option4, int answerValue){
        myQuestionText = questionText;
        myOption1 = option1;
        myOption2 = option2;
        myOption3 = option3;
        myOption4 = option4;
        myAnswerValue = answerValue;
    }

    public String getQuestionText() {
        return myQuestionText;
    }

    public void setQuestionText(String questionText) {
        myQuestionText = questionText;
    }

    public String getOption1() {
        return myOption1;
    }

    public void setOption1(String option1) {
        myOption1 = option1;
    }

    public String getOption2() {
        return myOption2;
    }

    public void setOption2(String option2) {
        myOption2 = option2;
    }

    public String getOption3() {
        return myOption3;
    }

    public void setOption3(String option3) {
        myOption3 = option3;
    }

    public String getOption4() {
        return myOption4;
    }

    public void setOption4(String option4) {
        myOption4 = option4;
    }

    public int getAnswerValue() {
        return myAnswerValue;
    }

    public void setAnswerValue(int answerValue) {
        myAnswerValue = answerValue;
    }
}
