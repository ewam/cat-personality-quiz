package ewa.catpersonalityquiz.Quizzes;

import ewa.catpersonalityquiz.Quizzes.Question;

/**
 * Created by Ewa on 8/29/2015.
 * Holds the actual quiz(zes).
 */
public class Quiz {
    public Question[] myQuestions;
    public int myQuizLength;
    public int myQuizResult;

    //rather than the standard getter, adds up all values that have been assigned to the quiz questions'
    //myAnswerValue and returns the total
    //TODO: check to see if this is bad programming etiquette and I should make a separate method
    public int getQuizResult() {
        myQuizResult = 0;
        for (int questionNumber = 0; questionNumber < myQuizLength; questionNumber++) {
            myQuizResult += myQuestions[questionNumber].getAnswerValue();
        }
        return myQuizResult;
    }

    public void setQuizResult(int quizResult) {
        myQuizResult = quizResult;
    }

    public Question getQuestion(int questionNmr){return myQuestions[questionNmr];}

    public int getQuizLength() {
        return myQuizLength;
    }

    public void setQuizLength(int quizLength) {
        myQuizLength = quizLength;
    }


    public Quiz(){
        myQuestions = new Question[10];
        myQuizLength = myQuestions.length;

        //quiz question constructors
        myQuestions[0] = new Question("You are in your room, chilling/working/performing eldritch rituals; %1$s:",
                "Actively (constantly) demands your attention",
                "Is around somewhere. Probably.",
                "Is in the same room, completely \"coincidentally,\" of course",
                "Nonchalantly walks/sleeps on top of whatever has your attention",
                0
        );

        myQuestions[1] = new Question("It's the middle of the night, and you are sleeping; %1$s:",
                "Is also sleeping, on top of you",
                "Is around somewhere. Probably.",
                "Is chasing invisible demons while making hellbeast noises",
                "Wants food. NOW.",
                0);

        myQuestions[2] = new Question("What does %1$s eat?",
                "Literally anything (and everything)",
                "They have a few favorites and refuse to east anything else",
                "Varies depending on mood, the time of the day, the phase of the moon...",
                "Whatever is most expensive. Or what you're eating, that works.",
                0);

        myQuestions[3] = new Question("You get a package.  How does %1$s react to the box?",
                "Tries to eat it",
                "Terrified or at least deeply suspicious of it",
                "Immediately sits/attempts to sit in it",
                "Tries to kill it",
                0);

        myQuestions[4] = new Question("How does %1$s treat visitors?",
                "With complete friendliness",
                "My visitors never even know I have a cat",
                "Rubs all over their stuff and then ignores them unless they have food.",
                "Oh god the blood",
                0);

        myQuestions[5] = new Question("What is %1$s's favorite toy?",
                "They love all of the toys I get them!",
                "The hoard of little plastic things from milk gallons hidden under the couch",
                "Not any cat toys that I actually bought, that's for sure",
                "Anything they can fall upon and claw into submission - including my feet",
                0);

        myQuestions[6] = new Question("How does %1$s react when you are away for longer periods of time?",
                "They're fine",
                "I don't want to talk about it",
                "I don't want to talk about it",
                "I don't want to talk about it",
                0);

        myQuestions[7] = new Question("What happens if you try to pet %1$s's belly?",
                "It's so...soft...",
                "...there are cats that allow this?",
                "They tolerate the petting for exactly 27.6 seconds and then leave",
                "Hah! As if I would fall for such an obvious trap",
                0
        );

        myQuestions[8] = new Question("How does %1$s show their displeasure with you/life/the invisible demons",
                "They sulk for a little while but quickly get over it",
                "They just...disappear. I have looked EVERYWHERE",
                "They suddenly \"forget\" where the litter box is",
                "I used to have nice shoes...and nice clothes...and breakables...",
                0);

        myQuestions[9] = new Question("Does %1$s respond to their name?",
                "Yes",
                "I have deluded myself into believing that they do",
                "Hahahahahaha. No.",
                "They will only answer to \"Death, the destroyer of worlds\"",
                0);



    }


}
