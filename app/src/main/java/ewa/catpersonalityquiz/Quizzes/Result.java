package ewa.catpersonalityquiz.Quizzes;

/**
 * Result object.  Contains member variables for quiz results, which are set in the result activity.
 */
public class Result {
    String myResultText;
    String myResultHeading;
    String myResultCategory;
    int myResultImageID;

    public String getResultCategory() {
        return myResultCategory;
    }

    public void setResultCategory(String resultCategory) {
        myResultCategory = resultCategory;
    }

    public String getResultText() {
        return myResultText;
    }

    public void setResultText(String resultText) {
        myResultText = resultText;
    }

    public String getResultHeading() {
        return myResultHeading;
    }

    public void setResultHeading(String resultHeading) {
        myResultHeading = resultHeading;
    }

    public int getResultImageID() {
        return myResultImageID;
    }

    public void setResultImageID(int resultImageID) {
        myResultImageID = resultImageID;
    }
}
